
package vue;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;
import javax.imageio.ImageIO;
import javax.swing.JComponent;
import modele.Case;
import modele.CaseType;

/**
 *
 * @author Saliba
 */
public class CaseView extends JComponent implements Serializable, Observer {

    private Case caseGrille;
   /**
     * Constructeur permettant d'initiliser une Case View réprésentant 
     * une case graphique.
     *
     * @param type le type de la case.
     */
    public CaseView(CaseType type) {
        this.caseGrille = new Case(type);
        // this.caseGrille.addObserver(this);
   //     System.out.println("observer ajouter");
        this.setMinimumSize(new Dimension(35, 35));
        this.setPreferredSize(new Dimension(35, 35));
        this.setEnabled(true);
        //  repaint();
    }
   /**
     * Fonction permettant de dessiner la case en fonction des différents
     * paramètres.
     *
     */
    @Override
    protected void paintComponent(Graphics g) {

        Dimension size = getSize();
        int width = size.width;
        int height = size.height;
        Image image;
        try {
            switch (this.getCaseGrille().getType()) {
                case CAISSE:
                    image = ImageIO.read(new File("./sources/caisse.png"));
                    ;
                    break;
                case MUR:
                    image = ImageIO.read(new File("./sources/mur.jpg"));
                    ;
                    break;
                case VIDE:
                    image = ImageIO.read(new File("./sources/chemin.jpg"));
                    ;
                    break;
                case PERSONNAGE:
                    image = ImageIO.read(new File("./sources/persoDroit.png"));
                    ;
                    break;
                case PERSONNAGESURCROIX:
                    image = ImageIO.read(new File("./sources/persoDroit.png"));
                    ;
                    break;
                case CAISSESURCROIX:
                    image = ImageIO.read(new File("./sources/caissesurcroix.png"));
                    ;
                    break;
                case CROIX:
                    image = ImageIO.read(new File("./sources/croix.png"));
                    ;
                    break;

                default:
                    image = ImageIO.read(new File("./sources/caiss.png"));

            }

        } catch (Exception ex) {
            image = null;
        }
        g.drawImage(image, 0, 0, width, height, this);

    }

    /**
     * Fonction permettant de retourner une  cases graphique.
     * @return the caseGrille
     */
    public Case getCaseGrille() {
        return caseGrille;
    }

    /**
     * Fonction permettant de modifier une cases graphique.
     * @param caseGrille la nouvelle case de la grille
     */
    public void setCaseGrille(Case caseGrille) {
        this.caseGrille = caseGrille;
        this.caseGrille.addObserver(this);
        repaint();
    }
    /**
     * Fonction permettant de modifier une case en fonction des changements
     * d'état d'une case.
     */
    @Override
    public void update(Observable o, Object arg) {
     //   System.out.println("Ma case view a changer");

        if (arg != null) {
            Case cas = (Case) arg;
            caseGrille.setType(cas.getType());
            repaint();
        }
    }
}
