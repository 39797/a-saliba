package vue;

import controleur.Controleur;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import modele.Niveau;

/**
 *
 * @author Saliba
 */
public class MainWindow extends javax.swing.JFrame implements PropertyChangeListener {

    private Controleur ctrl;
    private int compteurNiveau = 1;
    private Niveau niv;

    /**
     * Création de la fenêtre principale.
     */
    public MainWindow() {

        initComponents();
        // getContentPane().setBackground(Color.WHITE);  //Whatever color
        niv = new Niveau("./niveau/level" + compteurNiveau + ".txt", compteurNiveau);
        this.indicateurNiveau.setText(compteurNiveau + "");

        plateauView2.reni(niv.getP());
        ctrl = new Controleur(niv);

        ctrl.addPropertyChangeListener(this);
        this.pack();

        plateauView2.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), "monter");
        plateauView2.getActionMap().put("monter", new AbstractAction() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent decendre) {
                //    System.out.println("moonter");
                ctrl.monter();
            }
        });
        plateauView2.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), "decendre");
        plateauView2.getActionMap().put("decendre", new AbstractAction() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent decendre) {
                //    System.out.println("decendre");
                ctrl.decendre();
            }
        });
        plateauView2.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), "droite");
        plateauView2.getActionMap().put("droite", new AbstractAction() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent droite) {
                //     System.out.println("droite");
                ctrl.droite();
            }
        });
        plateauView2.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), "gauche");
        plateauView2.getActionMap().put("gauche", new AbstractAction() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent gauche) {
                //   System.out.println("gauche");
                ctrl.gauche();
            }
        });

    }

    /**
     * Fonction permettant de modifier l'état de la vue lorsqu'un changement
     * s'éffectue dans la plateau.
     *
     * @param event le changement.
     */
    @Override
    public void propertyChange(PropertyChangeEvent event) {
        if (event.getPropertyName().equals("checkPartie")) {

            compteurNiveau++;
            System.out.println("compteur " + compteurNiveau);
            if (compteurNiveau == 11) {
                compteurNiveau = 1;
            }
            this.indicateurNiveau.setText(compteurNiveau + "");
            niv = new Niveau("./niveau/level" + compteurNiveau + ".txt", compteurNiveau);

            plateauView2.reni(niv.getP());

            ctrl = new Controleur(niv);
            ctrl.addPropertyChangeListener(this);

            this.pack();

        }
        if (event.getPropertyName().equals("nombreMovement")) {
            this.nombreMovement.setText(event.getNewValue().toString());

        }
        if (event.getPropertyName().equals("nombreMovementmin")) {
            this.nombreMovementmin.setText(event.getNewValue().toString());
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        nivSuiv = new javax.swing.JButton();
        renitialiser = new javax.swing.JButton();
        plateauView2 = new vue.PlateauView();
        jLabel1 = new javax.swing.JLabel();
        nombreMovement = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        indicateurNiveau = new javax.swing.JLabel();
        nivPrece = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        nombreMovementmin = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jMenuBar2 = new javax.swing.JMenuBar();
        jMenu3 = new javax.swing.JMenu();
        ouvrir = new javax.swing.JMenuItem();
        sauvgarde = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();

        jMenu1.setText("File");
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        nivSuiv.setText("Niveau suivant");
        nivSuiv.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nivSuivActionPerformed(evt);
            }
        });

        renitialiser.setText("Réinitialiser");
        renitialiser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                renitialiserActionPerformed(evt);
            }
        });

        jLabel1.setText("Nombre de mouvements :");

        nombreMovement.setText("0");

        jLabel2.setText("Niveau :");

        indicateurNiveau.setText("0");

        nivPrece.setText("Niveau précédent");
        nivPrece.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nivPreceActionPerformed(evt);
            }
        });

        jLabel3.setText("Mouvements minimum :");

        nombreMovementmin.setText("0");

        jLabel4.setFont(new java.awt.Font("Verdana", 2, 24)); // NOI18N
        jLabel4.setText("Sokoban");

        jMenu3.setText("File");

        ouvrir.setText("Ouvrir sauvgarde");
        ouvrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ouvrirActionPerformed(evt);
            }
        });
        jMenu3.add(ouvrir);

        sauvgarde.setText("Sauvgarder");
        sauvgarde.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sauvgardeActionPerformed(evt);
            }
        });
        jMenu3.add(sauvgarde);

        jMenuItem1.setText("Fermer");
        jMenu3.add(jMenuItem1);

        jMenuBar2.add(jMenu3);

        setJMenuBar(jMenuBar2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(plateauView2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(nivSuiv, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(renitialiser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(nivPrece))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(indicateurNiveau))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nombreMovement)
                            .addComponent(nombreMovementmin, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addGap(28, 28, 28))
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel4)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(indicateurNiveau))
                        .addGap(8, 8, 8)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(nombreMovement))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(nombreMovementmin))
                        .addGap(28, 28, 28)
                        .addComponent(renitialiser)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(nivSuiv)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(nivPrece))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(plateauView2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(57, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Fonction permettant de réinitiliser un niveau, la fonction se déclenche 
     * par l'intermédiaire du boutton réinitiliser.
     *
     * @param evt l'évenement décencheur.
     */

    private void renitialiserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_renitialiserActionPerformed

        niv = new Niveau("./niveau/level" + compteurNiveau + ".txt", compteurNiveau);
        this.indicateurNiveau.setText(compteurNiveau + "");

        plateauView2.reni(niv.getP());

        ctrl = new Controleur(niv);
        ctrl.addPropertyChangeListener(this);

        this.nombreMovement.setText(0 + "");

        this.pack();


    }//GEN-LAST:event_renitialiserActionPerformed
    /**
     * Fonction permettant de passer à un niveau suppérieur, la fonction se
     * déclenche par l'intermédiaire du boutton Niveau suivant.
     *
     * @param evt l'évenement décencheur.
     */
    private void nivSuivActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nivSuivActionPerformed

        compteurNiveau++;
        System.out.println("compteur " + compteurNiveau);
        if (compteurNiveau == 21) {
            compteurNiveau = 1;
        }
        niv = new Niveau("./niveau/level" + compteurNiveau + ".txt", compteurNiveau);
        this.indicateurNiveau.setText(compteurNiveau + "");
        plateauView2.reni(niv.getP());

        ctrl = new Controleur(niv);
        ctrl.addPropertyChangeListener(this);

        this.nombreMovement.setText(0 + "");

        this.pack();
    }//GEN-LAST:event_nivSuivActionPerformed
/**
     * Fonction permettant de passer à un niveau précédent, la fonction se déclenche 
     * par l'intermédiaire du boutton Niveau précédent.
     *
     * @param evt l'évenement décencheur.
     */
    private void nivPreceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nivPreceActionPerformed
        compteurNiveau--;
        System.out.println("compteur " + compteurNiveau);
        if (compteurNiveau == 0) {
            compteurNiveau = 20;
        }
        niv = new Niveau("./niveau/level" + compteurNiveau + ".txt", compteurNiveau);
        this.indicateurNiveau.setText(compteurNiveau + "");
        plateauView2.reni(niv.getP());

        ctrl = new Controleur(niv);
        ctrl.addPropertyChangeListener(this);

        this.nombreMovement.setText(0 + "");

        this.pack();

    }//GEN-LAST:event_nivPreceActionPerformed
/**
     * Fonction permettant de sauvgarder une partie, la fonction se déclenche 
     * par l'intermédiaire du boutton menu sauvgarde.
     *
     * @param evt l'évenement décencheur.
     */
    private void sauvgardeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sauvgardeActionPerformed
        ctrl.save();
    }//GEN-LAST:event_sauvgardeActionPerformed
/**
     * Fonction permettant de récupérer la sauvgarde du niveau corespondant,
     * la fonction se déclenche par l'intermédiaire du boutton Niveau suivant.
     *
     * @param evt l'évenement décencheur.
     */
    private void ouvrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ouvrirActionPerformed
        niv = new Niveau("./sauvgarde/sokoSave" + compteurNiveau + ".txt", compteurNiveau);
        this.indicateurNiveau.setText(compteurNiveau + "");
        plateauView2.reni(niv.getP());

        ctrl = new Controleur(niv);
        ctrl.addPropertyChangeListener(this);

        this.nombreMovement.setText(0 + "");

        this.pack();
    }//GEN-LAST:event_ouvrirActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainWindow().setVisible(true);
            }
        });
    }
    private int xx = 1;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel indicateurNiveau;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuBar jMenuBar2;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JButton nivPrece;
    private javax.swing.JButton nivSuiv;
    private javax.swing.JLabel nombreMovement;
    private javax.swing.JLabel nombreMovementmin;
    private javax.swing.JMenuItem ouvrir;
    private vue.PlateauView plateauView2;
    private javax.swing.JButton renitialiser;
    private javax.swing.JMenuItem sauvgarde;
    // End of variables declaration//GEN-END:variables
}
