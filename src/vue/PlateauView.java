
package vue;

import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.JPanel;
import modele.Plateau;

/**
 *
 * @author Saliba
 */
public class PlateauView extends JPanel {
    private CaseView[][] grillebeans;
    private Plateau p;
  /**
     * Constructeur permettant d'initiliser un plateau de Case View graphique.
     *
     * @param partie le plateau de cases.
     */
    public PlateauView(Plateau partie) {
        this.p = partie;
        reni(p);
    }
/**
     * Constructeur sans paramètres permettant d'initiliser un plateau de Case View graphique.
     *
     */
    public PlateauView() {

    }

    /**
     * Fonction permettant de réinitialiser une grille graphique de case.
     *
     * @param p nouvelle grille de case
     */
    public void reni(Plateau p) {
        this.removeAll();
        this.setLayout(new GridLayout(p.getTailleMaxX(), p.getTailleMaxY()));
        grillebeans = new CaseView[p.getTailleMaxX()][p.getTailleMaxY()];
        this.setMinimumSize(new Dimension(p.getTailleMaxX() * 30, p.getTailleMaxY() * 30));
        this.setPreferredSize(new Dimension(p.getTailleMaxX() * 30, p.getTailleMaxY() * 30));
        for (int i = 0; i < p.getTailleMaxX(); i++) {
            for (int j = 0; j < p.getTailleMaxY(); j++) {
                grillebeans[i][j] = new CaseView(p.getGrille()[i][j].getType());
                p.getGrille()[i][j].addObserver(grillebeans[i][j]);
                this.add(grillebeans[i][j]);
            }
        }
    }
}
