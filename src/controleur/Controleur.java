
package controleur;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import modele.Ecriture;
import modele.Niveau;
import vue.PlateauView;

/**
 * @author Saliba
 */
public class Controleur {
    
    protected PropertyChangeSupport propertyChangeSupport;
    private boolean check = false;
    private Niveau niv;

    /**
     * Constructeur avec paramètres permettant d’initialiser un contrôleur.
     *
     * @param niv variable correspondant à un niveau
     */
    public Controleur(final Niveau niv) {
        this.niv = niv;

        propertyChangeSupport = new PropertyChangeSupport(this);
    }

    /**
     * Fonction permettant de déplacer le personnage vers la gauche.
     *
     */
    public void gauche() {
        boolean check = niv.getP().gauche();
        propertyChangeSupport.firePropertyChange("checkPartie", false, check);
        propertyChangeSupport.firePropertyChange("nombreMovement", niv.getP().getNombreMouvement() - 1, niv.getP().getNombreMouvement());
        propertyChangeSupport.firePropertyChange("nombreMovementmin", niv.getP().getNombreMouvementMin() - 10, niv.getP().getNombreMouvementMin());
    }

    /**
     * Fonction permettant de déplacer le personnage vers la droite.
     *
     */
    public void droite() {
        boolean check = niv.getP().droite();
        propertyChangeSupport.firePropertyChange("checkPartie", false, check);
        propertyChangeSupport.firePropertyChange("nombreMovement", niv.getP().getNombreMouvement() - 1, niv.getP().getNombreMouvement());
        propertyChangeSupport.firePropertyChange("nombreMovementmin", niv.getP().getNombreMouvementMin() - 10, niv.getP().getNombreMouvementMin());
    }

    /**
     * Fonction permettant de déplacer le personnage vers la le bas.
     *
     */
    public void decendre() {
        boolean check = niv.getP().decendre();
        propertyChangeSupport.firePropertyChange("checkPartie", false, check);
        propertyChangeSupport.firePropertyChange("nombreMovement", niv.getP().getNombreMouvement() - 1, niv.getP().getNombreMouvement());
        propertyChangeSupport.firePropertyChange("nombreMovementmin", niv.getP().getNombreMouvementMin() - 10, niv.getP().getNombreMouvementMin());

    }

    /**
     * Fonction permettant de déplacer le personnage vers la haut.
     *
     */
    public void monter() {
        boolean check = niv.getP().monter();
        propertyChangeSupport.firePropertyChange("checkPartie", false, check);
        propertyChangeSupport.firePropertyChange("nombreMovement", niv.getP().getNombreMouvement() - 1, niv.getP().getNombreMouvement());
        propertyChangeSupport.firePropertyChange("nombreMovementmin", niv.getP().getNombreMouvementMin() - 10, niv.getP().getNombreMouvementMin());

    }

    /**
     * Fonction permettant d'ajouter un listener
     *
     */
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    /**
     * Fonction permettant de connaître l'état d'une partie en cours.
     * 
     * @return boolean état de la partie
     */
    public boolean getFinPartie() {
        return check;
    }
 /**
     * Fonction permettant de sauvgarder une partie en cours. La partie
     * sauvgarder se trouve dans le dossier de sauvgarde.
     */
    public void save() {
        Ecriture e = new Ecriture(niv.getP());
        try {
            e.sauvgarder();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Controleur.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Controleur.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
