
package modele;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Saliba
 */
public class Lecture {

    private String path;
    private String contenu;

    /**
     * Constructeur permettant d'initiliser une lecture de niveau.
     *
     * @param path le chemin du niveau
     */
    public Lecture(String path) {
        this.path = path;
        this.contenu = ouvrirFichier(path);
    }

    /**
     * Fonction permettant d'ouvrir un fichier et récupérant son contenu.
     *
     * @param path le chemin du niveau.
     * @return String contenant le contenu du fichier.
     */
    private String ouvrirFichier(String path) {
        BufferedReader br = null;
        String ligneCourante, contenuGeneral = "";
        try {
            br = new BufferedReader(new FileReader(path));

            while ((ligneCourante = br.readLine()) != null) {
                contenuGeneral = contenuGeneral + "\n" + ligneCourante;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return contenuGeneral;
    }

    /**
     * Fonction permettant de retourner le chemin.
     * 
     * @return le chemin du fichier.
     */
    public String getPath() {
        return path;
    }

    /**
     * Fonction permettant de modifier le chemin
     * @param path le nouveau chemin
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * Fonction permettant de récupérer le contenu du fichier.
     * 
     * @return le contenu
     */
    public String getContenu() {
        return contenu;
    }

    /**
     * Fonction permettant de modifier le contenu récupérer.
     * 
     * @param contenu le nouveau contenu.
     */
    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

}
