
package modele;

/**
 *
 * @author Saliba
 */
public enum CaseType {
    
    VIDE,  
    CAISSE, 
    PERSONNAGE, 
    CROIX, 
    CAISSESURCROIX, 
    MUR, 
    PERSONNAGESURCROIX
}
