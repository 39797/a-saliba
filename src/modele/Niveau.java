
package modele;

import java.sql.SQLException;

/**
 *
 * @author Saliba
 */
public class Niveau {

    private Plateau p;

    /**
     * Constructeur permettant d'initiliser un niveau.
     *
     * @param path le chemin du niveau
     * @param numNiveau le numéro du niveau
     */
    public Niveau(String path, int numNiveau) {
        initPlateau(path, numNiveau);
    }

    /**
     * Fonction permettant d'initiliser un niveau.
     *
     * @param path le chemin du niveau
     * @param niv le numero du niveau
     */
    private void initPlateau(String path, int niv) {

        Lecture l = new Lecture(path);
        //  System.out.println(l.getContenu());
        int cptCol = 0, cptColMax = 0;
        int cptLig = 0;

        for (int i = 0; i < l.getContenu().length(); i++) {
            if (l.getContenu().charAt(i) == '\n') {
                cptLig++;
                // System.out.println(cptCol + " ligne suivante " + cptLig);
                if (cptCol > cptColMax) {
                    cptColMax = cptCol;
                }
                cptCol = 0;
            }
            cptCol++;

        }
        char[][] tab = new char[cptColMax + 1][cptLig + 1];
        cptCol = 0;
        cptLig = 0;
        //System.out.println("aaaa");
        for (int i = 0; i < l.getContenu().length(); i++) {
            if (l.getContenu().charAt(i) == '\n') {
                cptLig++;
                cptCol = 0;
            }
            tab[cptCol][cptLig] = l.getContenu().charAt(i);
            cptCol++;
        }
        try {
            setP(new Plateau(cptColMax, cptLig, tab));
            p.setNiveau(niv);
        } catch (SQLException ex) {
            System.out.println("Erreur sql");
        }
    }

    /**
     * Fonction permettant d'afficher une grille.
     */
    public void affiche() {
        for (int i = 0; i < p.getGrille()[i].length; i++) {
            for (int j = 0; j < p.getGrille().length; j++) {

                System.out.print(" " + p.getGrille()[j][i].getType());
            }
            System.out.println("\n");
        }
    }

    /**
     * Fonction permettant de retourner une plateau.
     * @return le plateau.
     */
    public Plateau getP() {
        return p;
    }

    /**
     * Fonction permettant de modifier le plateau.
     * @param p le nouveau plateau
     */
    public void setP(Plateau p) {
        this.p = p;
    }
}
