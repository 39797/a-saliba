
package modele;

/**
 *
 * @author Saliba
 */
public class Position {

    private int x, y;
   /**
     * Constructeur permettant d'initiliser une position.
     *
     * @param x la position x.
     * @param y la position y.
     * case.
     */
    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }
   /**
     * Constructeur permettant d'initiliser une position par défaut.
     */
    public Position() {
        this.x = 0;
        this.y = 0;
    }

    /**
     * Fonction permettant de récupérer la position x.
     * @return la position x
     */
    public int getX() {
        return x;
    }

  /**
     * Fonction permettant de modifier la position x.
     * @param x la position x
     */
    public void setX(int x) {
        this.x = x;
    }

   /**
     * Fonction permettant de récupérer la position y.
     * @return la position y
     */
    public int getY() {
        return y;
    }

   /**
     * Fonction permettant de modifier la position y.
     * @param y la position y
     */
    public void setY(int y) {
        this.y = y;
    }
    
}
