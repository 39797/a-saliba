
package modele;

import db.ConnectionFactory;
import db.DAOPlateau;
import java.sql.SQLException;

/**
 *
 * @author Saliba
 */
public class Plateau {

    private Case[][] grille;
    private Position posPersonnage;
    private int tailleMaxX, tailleMaxY;
    private int nombreMouvement = 0, nombreCroixMax = 0, niveau = 1, nombreMouvementMin = 0;
    private DAOPlateau daoPlateau;
    private ConnectionFactory c;

    /**
     * Constructeur permettant d'initiliser un plateau.
     *
     * @param x la taille maximum de la colonne des abcisses.
     * @param y la taille maximum de la colonne des ordonnées.
     * @param tab tableau de caractères permettant d'initiliser la grille de
     * case.
     */
    public Plateau(int x, int y, char[][] tab) throws SQLException {
        tailleMaxX = x;
        tailleMaxY = y;
        grille = new Case[x][y];
        nombreMouvement = 0;
        nombreCroixMax = 0;

        c = new ConnectionFactory();
        daoPlateau = new DAOPlateau();

        for (int i = 0; i < tailleMaxX; i++) {
            for (int j = 0; j < tailleMaxY; j++) {
                switch (tab[i][j]) {
                    case '#':
                        grille[i][j] = new Case(CaseType.MUR);
                        ;
                        break;
                    case '$':
                        grille[i][j] = new Case(CaseType.CAISSE);
                        ;
                        break;
                    case '.':
                        grille[i][j] = new Case(CaseType.CROIX);
                        nombreCroixMax++;
                        ;
                        break;
                    case '*':
                        grille[i][j] = new Case(CaseType.CAISSESURCROIX);
                        nombreCroixMax++;
                        ;
                        break;
                    case '@':
                        grille[i][j] = new Case(CaseType.PERSONNAGE);
                        posPersonnage = new Position(i, j);
                        ;
                        break;
                    case '+':
                        grille[i][j] = new Case(CaseType.PERSONNAGESURCROIX);
                        posPersonnage = new Position(i, j);
                        ;
                        break;
                    case ' ':
                        grille[i][j] = new Case(CaseType.VIDE);
                        ;
                        break;
                    default:
                        grille[i][j] = new Case(CaseType.VIDE);
                }
            }
        }
        System.out.println("Nombre croix max : " + nombreCroixMax);
    }

    /**
     * Fonction permettant de connaitre si une partie est terminée.
     *
     * @return un boolean vrai si gagnant
     */
    public boolean checkGagnant() {
        int compteurCaisseSurCroix = 0;
        for (int i = 0; i < tailleMaxX; i++) {
            for (int j = 0; j < tailleMaxY; j++) {
                if (getGrille()[i][j].getType() == CaseType.CAISSESURCROIX) {
                    compteurCaisseSurCroix++;
                }
            }
        }
        if (compteurCaisseSurCroix == nombreCroixMax) {
            try {
                daoPlateau.insertData(this, c.getConn());
            } catch (SQLException ex) {
                System.out.println("Erreur d'insertion");
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Fonction permettant de retourner la grille de cases.
     *
     * @return la grille
     */
    public Case[][] getGrille() {
        return grille;
    }

    /**
     * Fonction permettant de modifier la grille.
     *
     * @param grille la nouvelle grille.
     */
    public void setGrille(Case[][] grille) {
        this.grille = grille;
    }

    /**
     * Fontion permettant de modifier une case de la grille.
     *
     * @param c nouveau type de la case
     * @param x position x
     * @param y position y
     */
    public void setPos(CaseType c, int x, int y) {
        this.getGrille()[x][y].setType(c);
    }

    /**
     * Fontion permettant de déplacer le joueur vers la droite.
     *
     * @return un boolean vrais si la partie est terminée.
     */
    public boolean droite() {
        int currentY = posPersonnage.getY() + 1;

        if (this.getGrille()[posPersonnage.getX()][currentY - 1].getType() == CaseType.PERSONNAGE
                && this.getGrille()[posPersonnage.getX()][currentY].getType() == CaseType.VIDE) {
            setPos(CaseType.VIDE, posPersonnage.getX(), currentY - 1);
            posPersonnage.setY(currentY);
            setPos(CaseType.PERSONNAGE, posPersonnage.getX(), currentY);
            nombreMouvement++;

        }
//////////////-------------------------------------------------------------------------

        if (this.getGrille()[posPersonnage.getX()][currentY - 1].getType() == CaseType.PERSONNAGESURCROIX
                && this.getGrille()[posPersonnage.getX()][currentY].getType() == CaseType.CROIX) {
            setPos(CaseType.CROIX, posPersonnage.getX(), currentY - 1);
            posPersonnage.setY(currentY);
            setPos(CaseType.PERSONNAGESURCROIX, posPersonnage.getX(), currentY);
            nombreMouvement++;

        }
        if (this.getGrille()[posPersonnage.getX()][currentY - 1].getType() == CaseType.PERSONNAGESURCROIX
                && this.getGrille()[posPersonnage.getX()][currentY].getType() == CaseType.VIDE) {
            setPos(CaseType.CROIX, posPersonnage.getX(), currentY - 1);
            posPersonnage.setY(currentY);
            setPos(CaseType.PERSONNAGE, posPersonnage.getX(), currentY);
            nombreMouvement++;

        }
        if (this.getGrille()[posPersonnage.getX()][currentY].getType() == CaseType.CROIX) {
            setPos(CaseType.VIDE, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setY(currentY);
            setPos(CaseType.PERSONNAGESURCROIX, posPersonnage.getX(), currentY);
            nombreMouvement++;

        }

//-------------------------------------------------------------------------
        if (this.getGrille()[posPersonnage.getX()][currentY - 1].getType() == CaseType.PERSONNAGE
                && this.getGrille()[posPersonnage.getX()][currentY].getType() == CaseType.CAISSE
                && this.getGrille()[posPersonnage.getX()][currentY + 1].getType() == CaseType.CROIX) {
            setPos(CaseType.VIDE, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setY(currentY);
            setPos(CaseType.PERSONNAGE, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSESURCROIX, posPersonnage.getX(), posPersonnage.getY() + 1);
            nombreMouvement++;
        }
        if (this.getGrille()[posPersonnage.getX()][currentY - 1].getType() == CaseType.PERSONNAGESURCROIX
                && this.getGrille()[posPersonnage.getX()][currentY].getType() == CaseType.CAISSESURCROIX
                && this.getGrille()[posPersonnage.getX()][currentY + 1].getType() == CaseType.VIDE) {
            setPos(CaseType.CROIX, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setY(currentY);
            setPos(CaseType.PERSONNAGESURCROIX, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSE, posPersonnage.getX(), posPersonnage.getY() + 1);
            nombreMouvement++;

        }
        if (this.getGrille()[posPersonnage.getX()][currentY - 1].getType() == CaseType.PERSONNAGESURCROIX
                && this.getGrille()[posPersonnage.getX()][currentY].getType() == CaseType.CAISSESURCROIX
                && this.getGrille()[posPersonnage.getX()][currentY + 1].getType() == CaseType.CROIX) {
            setPos(CaseType.CROIX, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setY(currentY);
            setPos(CaseType.PERSONNAGESURCROIX, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSESURCROIX, posPersonnage.getX(), posPersonnage.getY() + 1);
            nombreMouvement++;

        }
        if (this.getGrille()[posPersonnage.getX()][currentY - 1].getType() == CaseType.PERSONNAGESURCROIX
                && this.getGrille()[posPersonnage.getX()][currentY].getType() == CaseType.CAISSE
                && this.getGrille()[posPersonnage.getX()][currentY + 1].getType() == CaseType.CROIX) {
            setPos(CaseType.CROIX, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setY(currentY);
            setPos(CaseType.PERSONNAGE, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSESURCROIX, posPersonnage.getX(), posPersonnage.getY() + 1);
            nombreMouvement++;

        }
        if (this.getGrille()[posPersonnage.getX()][currentY].getType() == CaseType.CAISSESURCROIX
                && this.getGrille()[posPersonnage.getX()][currentY + 1].getType() == CaseType.CROIX) {
            setPos(CaseType.VIDE, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setY(currentY);
            setPos(CaseType.PERSONNAGESURCROIX, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSESURCROIX, posPersonnage.getX(), posPersonnage.getY() + 1);
            nombreMouvement++;

        }
        if (this.getGrille()[posPersonnage.getX()][currentY - 1].getType() == CaseType.PERSONNAGESURCROIX
                && this.getGrille()[posPersonnage.getX()][currentY].getType() == CaseType.CAISSE
                && this.getGrille()[posPersonnage.getX()][currentY + 1].getType() == CaseType.VIDE) {
            setPos(CaseType.CROIX, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setY(currentY);
            setPos(CaseType.PERSONNAGE, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSE, posPersonnage.getX(), posPersonnage.getY() + 1);
            nombreMouvement++;

        }
        if (this.getGrille()[posPersonnage.getX()][currentY].getType() == CaseType.CAISSESURCROIX
                && this.getGrille()[posPersonnage.getX()][currentY + 1].getType() == CaseType.VIDE) {
            setPos(CaseType.VIDE, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setY(currentY);
            setPos(CaseType.PERSONNAGESURCROIX, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSE, posPersonnage.getX(), posPersonnage.getY() + 1);
            nombreMouvement++;

        }
        if (this.getGrille()[posPersonnage.getX()][currentY].getType() == CaseType.CAISSE
                && this.getGrille()[posPersonnage.getX()][currentY + 1].getType() == CaseType.VIDE) {
            setPos(CaseType.VIDE, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setY(currentY);
            setPos(CaseType.PERSONNAGE, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSE, posPersonnage.getX(), posPersonnage.getY() + 1);
            nombreMouvement++;

        }

        return checkGagnant();
    }

    /**
     * Fontion permettant de déplacer le joueur vers la gauche.
     *
     * @return un boolean vrais si la partie est terminée.
     */
    public boolean gauche() {

        int currentY = posPersonnage.getY() - 1;

        if (this.getGrille()[posPersonnage.getX()][currentY + 1].getType() == CaseType.PERSONNAGE
                && this.getGrille()[posPersonnage.getX()][currentY].getType() == CaseType.VIDE) {
            setPos(CaseType.VIDE, posPersonnage.getX(), currentY + 1);
            posPersonnage.setY(currentY);
            setPos(CaseType.PERSONNAGE, posPersonnage.getX(), currentY);
            nombreMouvement++;

        }
//////////////-------------------------------------------------------------------------

        if (this.getGrille()[posPersonnage.getX()][currentY + 1].getType() == CaseType.PERSONNAGESURCROIX
                && this.getGrille()[posPersonnage.getX()][currentY].getType() == CaseType.CROIX) {
            setPos(CaseType.CROIX, posPersonnage.getX(), currentY + 1);
            posPersonnage.setY(currentY);
            setPos(CaseType.PERSONNAGESURCROIX, posPersonnage.getX(), currentY);
            nombreMouvement++;

        }
        if (this.getGrille()[posPersonnage.getX()][currentY + 1].getType() == CaseType.PERSONNAGESURCROIX
                && this.getGrille()[posPersonnage.getX()][currentY].getType() == CaseType.VIDE) {
            setPos(CaseType.CROIX, posPersonnage.getX(), currentY + 1);
            posPersonnage.setY(currentY);
            setPos(CaseType.PERSONNAGE, posPersonnage.getX(), currentY);
            nombreMouvement++;

        }
        if (this.getGrille()[posPersonnage.getX()][currentY].getType() == CaseType.CROIX) {
            setPos(CaseType.VIDE, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setY(currentY);
            setPos(CaseType.PERSONNAGESURCROIX, posPersonnage.getX(), currentY);
            nombreMouvement++;

        }

//-------------------------------------------------------------------------
        if (this.getGrille()[posPersonnage.getX()][currentY + 1].getType() == CaseType.PERSONNAGE
                && this.getGrille()[posPersonnage.getX()][currentY].getType() == CaseType.CAISSE
                && this.getGrille()[posPersonnage.getX()][currentY - 1].getType() == CaseType.CROIX) {
            setPos(CaseType.VIDE, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setY(currentY);
            setPos(CaseType.PERSONNAGE, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSESURCROIX, posPersonnage.getX(), posPersonnage.getY() - 1);
            nombreMouvement++;

        }
        if (this.getGrille()[posPersonnage.getX()][currentY + 1].getType() == CaseType.PERSONNAGESURCROIX
                && this.getGrille()[posPersonnage.getX()][currentY].getType() == CaseType.CAISSESURCROIX
                && this.getGrille()[posPersonnage.getX()][currentY - 1].getType() == CaseType.VIDE) {
            setPos(CaseType.CROIX, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setY(currentY);
            setPos(CaseType.PERSONNAGESURCROIX, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSE, posPersonnage.getX(), posPersonnage.getY() - 1);
            nombreMouvement++;

        }
        if (this.getGrille()[posPersonnage.getX()][currentY + 1].getType() == CaseType.PERSONNAGESURCROIX
                && this.getGrille()[posPersonnage.getX()][currentY].getType() == CaseType.CAISSESURCROIX
                && this.getGrille()[posPersonnage.getX()][currentY + 1].getType() == CaseType.CROIX) {
            setPos(CaseType.CROIX, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setY(currentY);
            setPos(CaseType.PERSONNAGESURCROIX, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSESURCROIX, posPersonnage.getX(), posPersonnage.getY() - 1);
            nombreMouvement++;

        }
        if (this.getGrille()[posPersonnage.getX()][currentY + 1].getType() == CaseType.PERSONNAGESURCROIX
                && this.getGrille()[posPersonnage.getX()][currentY].getType() == CaseType.CAISSE
                && this.getGrille()[posPersonnage.getX()][currentY - 1].getType() == CaseType.CROIX) {
            setPos(CaseType.CROIX, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setY(currentY);
            setPos(CaseType.PERSONNAGE, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSESURCROIX, posPersonnage.getX(), posPersonnage.getY() - 1);
            nombreMouvement++;

        }
        if (this.getGrille()[posPersonnage.getX()][currentY].getType() == CaseType.CAISSESURCROIX
                && this.getGrille()[posPersonnage.getX()][currentY - 1].getType() == CaseType.CROIX) {
            setPos(CaseType.VIDE, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setY(currentY);
            setPos(CaseType.PERSONNAGESURCROIX, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSESURCROIX, posPersonnage.getX(), posPersonnage.getY() - 1);
            nombreMouvement++;

        }
        if (this.getGrille()[posPersonnage.getX()][currentY + 1].getType() == CaseType.PERSONNAGESURCROIX
                && this.getGrille()[posPersonnage.getX()][currentY].getType() == CaseType.CAISSE
                && this.getGrille()[posPersonnage.getX()][currentY - 1].getType() == CaseType.VIDE) {
            setPos(CaseType.CROIX, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setY(currentY);
            setPos(CaseType.PERSONNAGE, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSE, posPersonnage.getX(), posPersonnage.getY() - 1);
            nombreMouvement++;

        }
        if (this.getGrille()[posPersonnage.getX()][currentY].getType() == CaseType.CAISSESURCROIX
                && this.getGrille()[posPersonnage.getX()][currentY - 1].getType() == CaseType.VIDE) {
            setPos(CaseType.VIDE, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setY(currentY);
            setPos(CaseType.PERSONNAGESURCROIX, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSE, posPersonnage.getX(), posPersonnage.getY() - 1);
            nombreMouvement++;

        }
        if (this.getGrille()[posPersonnage.getX()][currentY].getType() == CaseType.CAISSE
                && this.getGrille()[posPersonnage.getX()][currentY - 1].getType() == CaseType.VIDE) {
            setPos(CaseType.VIDE, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setY(currentY);
            setPos(CaseType.PERSONNAGE, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSE, posPersonnage.getX(), posPersonnage.getY() - 1);
            nombreMouvement++;

        }

        return checkGagnant();
    }

    /**
     * Fontion permettant de déplacer le joueur vers le bas.
     *
     * @return un boolean vrais si la partie est terminée.
     */
    public boolean decendre() {

        int currentX = posPersonnage.getX() + 1;

        if (this.getGrille()[currentX - 1][posPersonnage.getY()].getType() == CaseType.PERSONNAGE
                && this.getGrille()[currentX][posPersonnage.getY()].getType() == CaseType.VIDE) {
            setPos(CaseType.VIDE, currentX - 1, posPersonnage.getY());
            posPersonnage.setX(currentX);
            setPos(CaseType.PERSONNAGE, currentX, posPersonnage.getY());
            nombreMouvement++;

        }
//////////////-------------------------------------------------------------------------

        if (this.getGrille()[currentX - 1][posPersonnage.getY()].getType() == CaseType.PERSONNAGESURCROIX
                && this.getGrille()[currentX][posPersonnage.getY()].getType() == CaseType.CROIX) {
            setPos(CaseType.CROIX, currentX - 1, posPersonnage.getY());
            posPersonnage.setX(currentX);
            setPos(CaseType.PERSONNAGESURCROIX, currentX, posPersonnage.getY());
            nombreMouvement++;

        }
        if (this.getGrille()[currentX - 1][posPersonnage.getY()].getType() == CaseType.PERSONNAGESURCROIX
                && this.getGrille()[currentX][posPersonnage.getY()].getType() == CaseType.VIDE) {
            setPos(CaseType.CROIX, currentX - 1, posPersonnage.getY());
            posPersonnage.setX(currentX);
            setPos(CaseType.PERSONNAGE, currentX, posPersonnage.getY());
            nombreMouvement++;

        }
        if (this.getGrille()[currentX][posPersonnage.getY()].getType() == CaseType.CROIX) {
            setPos(CaseType.VIDE, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setX(currentX);
            setPos(CaseType.PERSONNAGESURCROIX, posPersonnage.getX(), posPersonnage.getY());
            nombreMouvement++;

        }

//-------------------------------------------------------------------------
        if (this.getGrille()[currentX - 1][posPersonnage.getY()].getType() == CaseType.PERSONNAGE
                && this.getGrille()[currentX][posPersonnage.getY()].getType() == CaseType.CAISSE
                && this.getGrille()[currentX + 1][posPersonnage.getY()].getType() == CaseType.CROIX) {
            setPos(CaseType.VIDE, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setX(currentX);
            setPos(CaseType.PERSONNAGE, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSESURCROIX, posPersonnage.getX() + 1, posPersonnage.getY());
            nombreMouvement++;

        }
        if (this.getGrille()[currentX - 1][posPersonnage.getY()].getType() == CaseType.PERSONNAGESURCROIX
                && this.getGrille()[currentX][posPersonnage.getY()].getType() == CaseType.CAISSESURCROIX
                && this.getGrille()[currentX + 1][posPersonnage.getY()].getType() == CaseType.VIDE) {
            setPos(CaseType.CROIX, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setX(currentX);
            setPos(CaseType.PERSONNAGESURCROIX, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSE, posPersonnage.getX() + 1, posPersonnage.getY());
            nombreMouvement++;

        }
        if (this.getGrille()[currentX - 1][posPersonnage.getY()].getType() == CaseType.PERSONNAGESURCROIX
                && this.getGrille()[currentX][posPersonnage.getY()].getType() == CaseType.CAISSESURCROIX
                && this.getGrille()[currentX + 1][posPersonnage.getY()].getType() == CaseType.CROIX) {
            setPos(CaseType.CROIX, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setX(currentX);
            setPos(CaseType.PERSONNAGESURCROIX, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSESURCROIX, posPersonnage.getX() + 1, posPersonnage.getY());
            nombreMouvement++;

        }
        if (this.getGrille()[currentX - 1][posPersonnage.getY()].getType() == CaseType.PERSONNAGESURCROIX
                && this.getGrille()[currentX][posPersonnage.getY()].getType() == CaseType.CAISSE
                && this.getGrille()[currentX + 1][posPersonnage.getY()].getType() == CaseType.CROIX) {
            setPos(CaseType.CROIX, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setX(currentX);
            setPos(CaseType.PERSONNAGE, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSESURCROIX, posPersonnage.getX() + 1, posPersonnage.getY());
            nombreMouvement++;

        }
        if (this.getGrille()[currentX][posPersonnage.getY()].getType() == CaseType.CAISSESURCROIX
                && this.getGrille()[currentX + 1][posPersonnage.getY()].getType() == CaseType.CROIX) {
            setPos(CaseType.VIDE, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setX(currentX);
            setPos(CaseType.PERSONNAGESURCROIX, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSESURCROIX, posPersonnage.getX() + 1, posPersonnage.getY());
            nombreMouvement++;

        }
        if (this.getGrille()[currentX - 1][posPersonnage.getY()].getType() == CaseType.PERSONNAGESURCROIX
                && this.getGrille()[currentX][posPersonnage.getY()].getType() == CaseType.CAISSE
                && this.getGrille()[currentX + 1][posPersonnage.getY()].getType() == CaseType.VIDE) {
            setPos(CaseType.CROIX, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setX(currentX);
            setPos(CaseType.PERSONNAGE, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSE, posPersonnage.getX() + 1, posPersonnage.getY());
            nombreMouvement++;

        }
        if (this.getGrille()[currentX][posPersonnage.getY()].getType() == CaseType.CAISSESURCROIX
                && this.getGrille()[currentX + 1][posPersonnage.getY()].getType() == CaseType.VIDE) {
            setPos(CaseType.VIDE, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setX(currentX);
            setPos(CaseType.PERSONNAGESURCROIX, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSE, posPersonnage.getX() + 1, posPersonnage.getY());
            nombreMouvement++;

        }
        if (this.getGrille()[currentX][posPersonnage.getY()].getType() == CaseType.CAISSE
                && this.getGrille()[currentX + 1][posPersonnage.getY()].getType() == CaseType.VIDE) {
            setPos(CaseType.VIDE, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setX(currentX);
            setPos(CaseType.PERSONNAGE, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSE, posPersonnage.getX() + 1, posPersonnage.getY());
            nombreMouvement++;

        }

        return checkGagnant();
    }

    /**
     * Fontion permettant de déplacer le joueur vers le haut.
     *
     * @return un boolean vrais si la partie est terminée.
     */
    public boolean monter() {

        int currentX = posPersonnage.getX() - 1;

        if (this.getGrille()[currentX + 1][posPersonnage.getY()].getType() == CaseType.PERSONNAGE
                && this.getGrille()[currentX][posPersonnage.getY()].getType() == CaseType.VIDE) {
            setPos(CaseType.VIDE, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setX(currentX);
            setPos(CaseType.PERSONNAGE, posPersonnage.getX(), posPersonnage.getY());
            nombreMouvement++;

        }

//--------------------------------------------------------------------------------------       
        if (this.getGrille()[currentX + 1][posPersonnage.getY()].getType() == CaseType.PERSONNAGESURCROIX
                && this.getGrille()[currentX][posPersonnage.getY()].getType() == CaseType.VIDE) {
            setPos(CaseType.CROIX, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setX(currentX);
            setPos(CaseType.PERSONNAGE, posPersonnage.getX(), posPersonnage.getY());
            nombreMouvement++;

        }

        if (this.getGrille()[currentX + 1][posPersonnage.getY()].getType() == CaseType.PERSONNAGESURCROIX
                && this.getGrille()[currentX][posPersonnage.getY()].getType() == CaseType.CROIX) {
            setPos(CaseType.CROIX, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setX(currentX);
            setPos(CaseType.PERSONNAGESURCROIX, posPersonnage.getX(), posPersonnage.getY());
            nombreMouvement++;

        }

        if (this.getGrille()[currentX][posPersonnage.getY()].getType() == CaseType.CROIX) {
            setPos(CaseType.VIDE, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setX(currentX);
            setPos(CaseType.PERSONNAGESURCROIX, posPersonnage.getX(), posPersonnage.getY());
            nombreMouvement++;

        }
//--------------------------------------------------------------------------------------        
        if (this.getGrille()[currentX + 1][posPersonnage.getY()].getType() == CaseType.PERSONNAGE
                && this.getGrille()[currentX][posPersonnage.getY()].getType() == CaseType.CAISSE
                && this.getGrille()[currentX - 1][posPersonnage.getY()].getType() == CaseType.CROIX) {
            setPos(CaseType.VIDE, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setX(currentX);
            setPos(CaseType.PERSONNAGE, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSESURCROIX, posPersonnage.getX() - 1, posPersonnage.getY());
            nombreMouvement++;

        }
        if (this.getGrille()[currentX + 1][posPersonnage.getY()].getType() == CaseType.PERSONNAGESURCROIX
                && this.getGrille()[currentX][posPersonnage.getY()].getType() == CaseType.CAISSESURCROIX
                && this.getGrille()[currentX - 1][posPersonnage.getY()].getType() == CaseType.VIDE) {
            setPos(CaseType.CROIX, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setX(currentX);
            setPos(CaseType.PERSONNAGESURCROIX, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSE, posPersonnage.getX() - 1, posPersonnage.getY());
            nombreMouvement++;

        }
        if (this.getGrille()[currentX + 1][posPersonnage.getY()].getType() == CaseType.PERSONNAGESURCROIX
                && this.getGrille()[currentX][posPersonnage.getY()].getType() == CaseType.CAISSESURCROIX
                && this.getGrille()[currentX - 1][posPersonnage.getY()].getType() == CaseType.CROIX) {
            setPos(CaseType.CROIX, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setX(currentX);
            setPos(CaseType.PERSONNAGESURCROIX, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSESURCROIX, posPersonnage.getX() - 1, posPersonnage.getY());
            nombreMouvement++;

        }
        if (this.getGrille()[currentX + 1][posPersonnage.getY()].getType() == CaseType.PERSONNAGESURCROIX
                && this.getGrille()[currentX][posPersonnage.getY()].getType() == CaseType.CAISSE
                && this.getGrille()[currentX - 1][posPersonnage.getY()].getType() == CaseType.CROIX) {
            setPos(CaseType.CROIX, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setX(currentX);
            setPos(CaseType.PERSONNAGE, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSESURCROIX, posPersonnage.getX() - 1, posPersonnage.getY());
            nombreMouvement++;

        }
        if (this.getGrille()[currentX][posPersonnage.getY()].getType() == CaseType.CAISSESURCROIX
                && this.getGrille()[currentX - 1][posPersonnage.getY()].getType() == CaseType.CROIX) {
            setPos(CaseType.VIDE, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setX(currentX);
            setPos(CaseType.PERSONNAGESURCROIX, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSESURCROIX, posPersonnage.getX() - 1, posPersonnage.getY());
            nombreMouvement++;

        }
        if (this.getGrille()[currentX + 1][posPersonnage.getY()].getType() == CaseType.PERSONNAGESURCROIX
                && this.getGrille()[currentX][posPersonnage.getY()].getType() == CaseType.CAISSE
                && this.getGrille()[currentX - 1][posPersonnage.getY()].getType() == CaseType.VIDE) {
            setPos(CaseType.CROIX, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setX(currentX);
            setPos(CaseType.PERSONNAGE, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSE, posPersonnage.getX() - 1, posPersonnage.getY());
            nombreMouvement++;

        }
        if (this.getGrille()[currentX][posPersonnage.getY()].getType() == CaseType.CAISSESURCROIX
                && this.getGrille()[currentX - 1][posPersonnage.getY()].getType() == CaseType.VIDE) {
            setPos(CaseType.VIDE, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setX(currentX);
            setPos(CaseType.PERSONNAGESURCROIX, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSE, posPersonnage.getX() - 1, posPersonnage.getY());
            nombreMouvement++;

        }
        if (this.getGrille()[currentX][posPersonnage.getY()].getType() == CaseType.CAISSE
                && this.getGrille()[currentX - 1][posPersonnage.getY()].getType() == CaseType.VIDE) {
            setPos(CaseType.VIDE, posPersonnage.getX(), posPersonnage.getY());
            posPersonnage.setX(currentX);
            setPos(CaseType.PERSONNAGE, posPersonnage.getX(), posPersonnage.getY());
            setPos(CaseType.CAISSE, posPersonnage.getX() - 1, posPersonnage.getY());
            nombreMouvement++;

        }

        return checkGagnant();
    }

    /**
     * Fonction permettant de récupérer la taille maximum des abcisses
     *
     * @return the tailleMaxX
     */
    public int getTailleMaxX() {
        return tailleMaxX;
    }

    /**
     * Fonction permettant de récupérer la taille maximum des ordonnées
     *
     * @return the tailleMaxY
     */
    public int getTailleMaxY() {
        return tailleMaxY;
    }

    /**
     * Fonction permettant de récupérer le nombre de mouvement
     *
     * @return the nombreMouvement
     */
    public int getNombreMouvement() {
        return nombreMouvement;
    }

    /**
     * Fonction permettant de modifier le nombre de mouvement
     *
     * @param nombreMouvement le nouveau nombre de mouvement
     */
    public void setNombreMouvement(int nombreMouvement) throws SQLException {
        this.nombreMouvement = nombreMouvement;
        nombreMouvementMin = daoPlateau.getData(this, c.getConn());

    }

    /**
     * Fonction permettant de récupérer le niveau.
     *
     * @return le niveau
     */
    public int getNiveau() {
        return niveau;
    }

    /**
     * Fonction permettant de modifier le niveau
     *
     * @param niveau numéro du niveau.
     */
    public void setNiveau(int niveau) {
        this.niveau = niveau;
    }

    /**
     * Fonction permettant de récupérer le nombre de mouvement minimum
     * @return the nombreMouvementMin
     */
    public int getNombreMouvementMin() {
        return nombreMouvementMin;
    }

    /**
     * Fonction permettant de modifier le nombre de mouvement minimum
     * @param nombreMouvementMin le nouveau nombre de mouvement minimum
     */
    public void setNombreMouvementMin(int nombreMouvementMin) {
        this.nombreMouvementMin = nombreMouvementMin;
    }
}
