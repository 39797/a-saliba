
package modele;

import java.io.Serializable;
import java.util.Observable;

/**
 *
 * @author Saliba
 */
public class Case extends Observable implements Serializable {

    private CaseType type;
    private int x, y;

    /**
     * Constructeur permettant d'initialiser une case.
     *
     * @param type le type de la case
     *
     */
    public Case(CaseType type) {
        this.type = type;
    }

    /**
     * Fonction permettant de récupèrer le type de la case.
     *
     * @return le type de la case
     */
    public CaseType getType() {
        return type;
    }

    /**
     * Fonction permettant de modifier le type de la case.
     *
     * @param type le type de la case
     */
    public void setType(CaseType type) {
        this.type = type;
        this.setChanged();
        this.notifyObservers(this);
    }

    /**
     * Fonction permettant de notifier un observateur de la case.
     *
     */
    public void notifyPlateau() {
        this.setChanged();
        this.notifyObservers(this);
    }

}
