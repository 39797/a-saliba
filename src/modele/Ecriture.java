
package modele;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

/**
 *
 * @author Saliba
 */
public class Ecriture {

    private Plateau p;
  /**
     * Constructeur permettant d'initiliser une écriture de fichier pour le 
     * stockage.
     *  
     * @param partie la partie correspondant à un plateau de jeu.
     */
    public Ecriture(Plateau partie) {
        this.p = partie;
    }
  /**
     * Fonction permettant de sauvgarder l'état d'une partie en stockant les
     * données dans un fichier texte en parcourant le plateau.
     *  
     */
    public void sauvgarder() throws FileNotFoundException, UnsupportedEncodingException {
        PrintWriter writer = new PrintWriter("./sauvgarde/sokoSave"+p.getNiveau()+".txt", "UTF-8");
        for (int j = 0; j < p.getTailleMaxY(); j++) {
                String s = "";
            for (int i = 0; i < p.getTailleMaxX(); i++) {
                switch (p.getGrille()[i][j].getType()) {
                    case CAISSE:
                        s = s + "$";
                        break;
                    case MUR:
                        s = s + "#";
                        break;
                    case VIDE:
                        s = s + " ";
                        break;
                    case PERSONNAGE:
                        s = s + "@";
                        break;
                    case PERSONNAGESURCROIX:
                        s = s + "+";
                        break;
                    case CAISSESURCROIX:
                        s = s + "*";
                        break;
                    case CROIX:
                        s = s + ".";
                        break;
                    default:
                        s = s + "";
                }
            }
            writer.println(s);
        }
        writer.close();
    }

}
