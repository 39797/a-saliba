
package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Saliba
 */
public class ConnectionFactory {

    Connection cn = null;
    /**
     * Fonction permettant d'établir une connexion avec la base de données
     *
     *@return une connexion DB
     *@throws en cas de problème de connexion la fonction déclanche une exception.
     */
    public Connection getConn() throws SQLException {
        try {
            cn = DriverManager.getConnection("jdbc:derby://localhost:1527/Sokoban", "alg", "alg");
        } catch (SQLException se) {
            System.out.println("Connexion DB impossible");
        }
        return cn;
    }
}
