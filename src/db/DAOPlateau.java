/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modele.Plateau;

/**
 *
 * @author Saliba
 */
public class DAOPlateau {

    PreparedStatement ps = null;
    String sql = "insert into partie(niveau,nombremov) values(?,?)";
    String sql2 = "update partie set nombremov=? where niveau=?";
    String sql3 = "select MIN(nombremov)as mov from partie where niveau=?";

    /**
     * Fonction permettant d'insérer les informations concernant une partie dans
     * la base de données.
     *
     */
    public void insertData(Plateau plateauDto, Connection cn) {
        try {

            ps = cn.prepareStatement(sql);
            ps.setInt(1, plateauDto.getNiveau());
            ps.setInt(2, plateauDto.getNombreMouvement());
            ps.executeUpdate();
            cn.commit();
        } catch (SQLException se) {
            System.out.println("Execution du insert impossible");
        }
    }

    /**
     * Fonction permettant de modifier les informations concernant une partie
     * dans la base de données.
     *
     */
    public void updateData(Plateau plateauDto, Connection cn) {
        try {
            ps = cn.prepareStatement(sql2);
            ps.setInt(1, plateauDto.getNombreMouvement());
            ps.setInt(2, plateauDto.getNiveau());
            ps.executeUpdate();
            cn.commit();
        } catch (SQLException se) {
            System.out.println("Execution du update impossible");
        }
    }

    /**
     * Fonction permettant de récupérer les informations concernant une partie
     * dans la base de données.
     * @return int le nombre de mouvements minimum d'un niveau
     */
    public int getData(Plateau plateauDto, Connection cn) throws SQLException {
        ps = cn.prepareStatement(sql3);
        ps.setInt(1, plateauDto.getNiveau());

        ResultSet rs = ps.executeQuery();
        int mov = 0;
        while (rs.next()) {
            mov = rs.getInt("mov");
        }
        return mov;
    }

}
